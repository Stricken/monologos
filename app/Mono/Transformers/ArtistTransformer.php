<?php namespace Mono\Transformers;

class ArtistTransformer extends Transformer {

    /**
     * @param $artist
     * @return array
     */
    public function transform($artist)
    {
        return [
            'artist_id'         => $artist['id'],
            'artist_name'       => $artist['name'],
            'artist_face'       => $artist['face_img'],
            'artist_background' => $artist['background_img'],
            'artist_hometown'   => $artist['hometown'],
            'artist_birth'      => $artist['birth'],
            'artist_language'   => [
                'language_name' => $artist['language']['name'],
                'language_code' => $artist['language']['code'],
                'language_flag' => $artist['language']['flag'],
            ]
        ];
    }

    /**
     * @param $artist
     * @return array
     */
    public function transformWithVideos($artist)
    {

        $videos = array_map([$this, 'transformVideo'], $artist['videos']);

        return [
            'artist_id'         => $artist['id'],
            'artist_name'       => $artist['name'],
            'artist_face'       => $artist['face_img'],
            'artist_background' => $artist['background_img'],
            'artist_hometown'   => $artist['hometown'],
            'artist_birth'      => $artist['birth'],
            'artist_language'   => [
                'language_name' => $artist['language']['name'],
                'language_code' => $artist['language']['code'],
                'language_flag' => $artist['language']['flag'],
            ],
            'artist_videos'     => $videos
        ];
    }

    private function transformVideo($video)
    {
        return [
            'video_title' => $video['title'],
            'youtube_id'  => $video['youtube_id'],
        ];
    }
}