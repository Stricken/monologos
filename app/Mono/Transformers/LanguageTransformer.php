<?php namespace Mono\Transformers;

class LanguageTransformer extends Transformer {

    /**
     * @param $artist
     * @return array
     */
    public function transform($artist)
    {
        return [
            'language_id'   => $artist['id'],
            'language_name' => $artist['name'],
            'language_code' => $artist['code'],
            'language_flag' => $artist['flag'],
        ];
    }

}