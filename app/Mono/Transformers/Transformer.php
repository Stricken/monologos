<?php namespace Mono\Transformers;

abstract class Transformer {

    /**
     * Transforms a collection
     * @param $items
     * @return array
     */
    public function transformCollection($items)
    {
        return array_map([$this, 'transform'], $items);
    }

    /**
     * @param $item
     * @return array
     */
    public abstract function transform($item);
}