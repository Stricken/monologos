<?php namespace Mono\Transformers;

class VideoTransformer extends Transformer {

    /**
     * @param $artist
     * @return array
     */
    public function transform($artist)
    {
        return [
            'video_title' => $artist['title'],
            'youtube_id'  => $artist['youtube_id'],
        ];
    }
}