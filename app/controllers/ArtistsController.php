<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Mono\Transformers\ArtistTransformer;
use Mono\Transformers\VideoTransformer;

class ArtistsController extends \ApiController {

    protected $artistTransformer;
    protected $videoTransformer;

    function __construct(ArtistTransformer $artistTransformer, VideoTransformer $videoTransformer)
    {
        $this->artistTransformer = $artistTransformer;
        $this->videoTransformer = $videoTransformer;
    }

    /**
	 * Display a listing of the resource.
	 * GET /artists
	 *
	 * @return Response
	 */
	public function index()
	{
        $artists = Artist::with('language')->get();

        return $this->respond([
            'data' => $this->artistTransformer->transformCollection($artists->toArray())
        ]);
	}

    /**
     * Display a listing of the resource.
     * GET /artists/{id}/videos
     *
     * @param $id
     * @return Response
     */
    public function videos($id)
    {
        $videos = Video::whereArtistId($id)->get();

        if(!$videos)
        {
            return $this->respondNotFound('Artist does not exist.');
        }

        return $this->respond([
            'data' => $this->videoTransformer->transformCollection($videos->toArray())
        ]);
    }

    /**
     * Display a listing of the resource.
     * GET /artists/{id}/videos
     *
     * @param $id
     * @return Response
     */
    public function full($id)
    {
        try {
            $artist = Artist::with(['language', 'videos'])->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound('Artist does not exist.');
        }

        return $this->respond([
            'data' => $this->artistTransformer->transformWithVideos($artist->toArray())
        ]);
    }

	/**
	 * Show the form for creating a new resource.
	 * GET /artists/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /artists
	 *
	 * @return Response
	 */
	public function store()
	{
        return Artist::create(Input::all());
    }

	/**
	 * Display the specified resource.
	 * GET /artists/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        try {
            $artist = Artist::with('language')->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound('Artist does not exist.');
        }

        return $this->respond([
           'data' => $this->artistTransformer->transform($artist->toArray())
        ]);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /artists/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /artists/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /artists/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Artist::find($id)->delete();
	}
}