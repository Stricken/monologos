<?php

use Mono\Transformers\LanguageTransformer;

class LanguagesController extends \ApiController {

    protected $languagesTransformer;

    function __construct(LanguageTransformer $languagesTransformer)
    {
        $this->languagesTransformer = $languagesTransformer;
    }

	/**
	 * Display a listing of the resource.
	 * GET /language
	 *
	 * @return Response
	 */
	public function index()
	{
        $languages = Language::all();

        return $this->respond([
            'data' => $this->languagesTransformer->transformCollection($languages->toArray())
        ]);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /language/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /language
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /language/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /language/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /language/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /language/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}