<?php

use Faker\Factory as Faker;

class ArtistsTableSeeder extends Seeder {

    public function run()
    {

        $faker = Faker::create();

        foreach (Language::all() as $language) {
            foreach (range(1, 10) as $index) {
                Artist::create([
                    'name'           => $faker->name,
                    'background_img' => $faker->imageUrl($width = 640, $height = 480),
                    'face_img'       => $faker->imageUrl($width = 250, $height = 250),
                    'hometown'       => $faker->country,
                    'birth'          => $faker->date(),
                    'language_id'    => $language->id
                ]);
            }
        }

    }

}