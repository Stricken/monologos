<?php

use Faker\Factory as Faker;

class LanguagesTableSeeder extends Seeder {

    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 5) as $index) {
            Language::create([
                'name' => $faker->word,
                'code' => $faker->unique()->languageCode,
                'flag' => $faker->imageUrl($width = 32, $height = 32),
            ]);
        }
    }

}