<?php

use Faker\Factory as Faker;

class VideosTableSeeder extends Seeder {

    public function run()
    {
        $faker = Faker::create();

        foreach (Artist::all() as $artist) {
            foreach (range(1, 10) as $index) {
                Video::create([
                    'youtube_id' => $faker->randomNumber(),
                    'artist_id'  => $artist->id
                ]);
            }
        }
    }

}