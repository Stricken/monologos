<?php

class Artist extends \Eloquent {

    protected $fillable = ['name', 'background_img', 'face_img', 'hometown', 'birth', 'language_id'];
    public $timestamps = false;

    public function language()
    {
        return $this->belongsTo('Language');
    }

    public function videos()
    {
        return $this->hasMany('Video');
    }
}