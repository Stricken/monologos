<?php

class Video extends \Eloquent {

    protected $fillable = ['youtube_id','title','artist_id'];
    public $timestamps = false;
}