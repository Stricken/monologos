<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function () {
    return View::make('main');
});

Route::group(array('prefix' => 'api/v1'), function () {
    Route::get('artists/{id}/videos', 'ArtistsController@videos');
    Route::get('artists/{id}/full', 'ArtistsController@full');
    Route::resource('artists', 'ArtistsController');
    Route::resource('videos', 'VideosController');
    Route::resource('languages', 'LanguagesController');
});