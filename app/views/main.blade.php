<!DOCTYPE html>
<html ng-app>
<head>
    <meta charset="UTF-8">
    <title>Monólogos</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
</head>
<body>

<div class="container">

    <h1>Monólogos APP</h1>

    <div class="row" style="margin-top:45px; margin-bottom:45px">
        <div class="col-lg-6">
            <button class="btn btn-info btn-block" ng-click="showArtists = !showArtists">Artists</button>
        </div>
        <div class="col-lg-6">
            <button class="btn btn-info btn-block" ng-click="showLanguages = !showLanguages">Languages</button>
        </div>
    </div>

    <div id="artists" ng-controller="ArtistsController" ng-show="showArtists">
        <h1>Artists</h1>

        <div class="form-group">
            <select class="form-control" ng-model="artist">
                <option ng-repeat="artist in artists" value="@{{ artist.artist_id }}">@{{ artist.artist_name }}</option>
            </select>
        </div>

        <h1>Add Artist</h1>

        <form ng-submit="addArtist()">
            <div class="form-group">
                <input type="text" placeholder="Name" ng-model="newArtistName" class="form-control">
            </div>
            <div class="form-group">
                <input type="text" placeholder="Background" ng-model="newArtistBackground" class="form-control">
            </div>
            <div class="form-group">
                <input type="text" placeholder="Face" ng-model="newArtistFace" class="form-control">
            </div>
            <div class="form-group">
                <input type="text" placeholder="Hometown" ng-model="newArtistHometown" class="form-control">
            </div>
            <div class="form-group">
                <input type="text" placeholder="Birth" ng-model="newArtistBirth" class="form-control">
            </div>

            <div class="form-group">
                <select class="form-control" ng-model="newArtistLanguage">
                    <option ng-repeat="language in languages" value="@{{ language.language_id }}">@{{ language.language_name }}</option>
                </select>
            </div>

            <button type="submit" class="btn btn-success">Add artist</button>
        </form>

        <h1>New Video</h1>

        <form ng-submit="addVideo()">
            <div class="form-group">
                <input type="text" placeholder="Title" ng-model="newVideoTitle" class="form-control">
            </div>

            <div class="form-group">
                <input type="text" placeholder="Youtube ID" ng-model="newVideoYoutubeId" class="form-control">
            </div>

            <div class="form-group">
                <select class="form-control" ng-model="newVideoArtistId">
                    <option ng-repeat="artist in artists" value="@{{ artist.artist_id }}">@{{ artist.artist_name }}</option>
                </select>
            </div>

            <button type="submit" class="btn btn-success">Add video</button>
        </form>

    </div>

    <div id="languages" ng-controller="LanguagesController" ng-show="showLanguages">
    </div>

</div>

<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.21/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="/js/main.js"></script>
</body>
</html>