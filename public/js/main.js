function ArtistsController($scope, $http) {

    $http.get('/api/v1/artists').success(function (artists) {
        $scope.artists = artists.data;
    });

    $http.get('/api/v1/languages').success(function (languages) {
        $scope.languages = languages.data;
    });

    $scope.addArtist = function () {
        var artist = {
            name: $scope.newArtistName,
            background_img: $scope.newArtistBackground,
            face_img: $scope.newArtistFace,
            hometown: $scope.newArtistHometown,
            birth: $scope.newArtistBirth,
            language_id: $scope.newArtistLanguage
        }

        $scope.artists.push(artist);

        $http.post('/api/v1/artists', artist).success(function () {
            alert('New artist added');
        }).error(function () {
            alert('Error');
        });
    }

    $scope.addVideo = function () {
        var video = {
            title: $scope.newVideoTitle,
            youtube_id: $scope.newVideoYoutubeId,
            artist_id: $scope.newVideoArtistId
        }

        $http.post('/api/v1/videos', video).success(function () {
            alert('New video added');
        }).error(function () {
            alert('Error');
        });
    }

    $scope.deleteVideo = function () {
        var video = {
            id: $scope.deleteVideoId
        }

        $http.delete('/api/v1/videos/' + video.id).success(function () {
            alert('Video deleted');
        }).error(function () {
            alert('Error');
        });
    }
}

function LanguagesController($scope, $http) {
    $http.get('/api/v1/languages').success(function (languages) {
        $scope.languages = languages.data;
    });
}